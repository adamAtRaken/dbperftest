# DB Performance Tester

Measures the duration of some DB queries, based on a randomized parameters and 20 runs.  This will show us if we're making changes for the better or not!

## To Build

Change into the root directory and run:
`mvn clean package`

This builds an executable jar for you.

## To Run

This is set up to run against Develop by default, but you can provide the Spring Environment on the command line to run against test.

For example, `java -jar target/DbPerfTester-0.0.1-SNAPSHOT.jar production` to run against Prod or `java -jar target/DbPerfTester-0.0.1-SNAPSHOT.jar` to test the queries on develop.  

## Reults

I have put the first results files into the results directory - let's run this periodically to see how we're going.  I will try and graph this too to give a more visual touch.