package com.rakenapp.dbperf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import com.orbitz.consul.Consul;
import com.orbitz.consul.KeyValueClient;
import com.rakenapp.dbperf.checks.AbstractPerformanceTest;
import com.rakenapp.dbperf.checks.CountMonthlyActiveUsersTest;
import com.rakenapp.dbperf.checks.FindFirstNewJobTest;
import com.rakenapp.dbperf.checks.FindUserByUuidTest;
import com.rakenapp.dbperf.checks.InvoiceIndexesTest;
import com.rakenapp.dbperf.checks.ListReportsTest;
import com.rakenapp.dbperf.checks.MediaCountTest;

public class DbPerfTester {
  private static final String CONSUL_URL = "http://consul-dev.rakenapp.com:8500/";

  private List<AbstractPerformanceTest> tests = new ArrayList<>();
  
  private String dbUrl;
  private String user;
  private String password;
  
  public DbPerfTester(String env) throws Exception {
    getDbValues(env);
  }

  public static void main(String args[]) throws Exception {
    String env = "dev";
    if(args.length > 0) {
      env = args[0];
    }
    System.out.println("Using Consul env " + env);
    DbPerfTester tester = new DbPerfTester(env);
    
    // Add all the tests
    tester.addTest(new CountMonthlyActiveUsersTest());
    tester.addTest(new FindFirstNewJobTest());
    tester.addTest(new FindUserByUuidTest());
    tester.addTest(new InvoiceIndexesTest());
    tester.addTest(new ListReportsTest());
    tester.addTest(new MediaCountTest());

    tester.runTests();
  }

  
  private void addTest(AbstractPerformanceTest test) {
    tests.add(test);
  }

  
  private void getDbValues(String env) {
    // The Dev DB URL is not in Consul?!?
    if(env.equals("dev")) {
      dbUrl = "jdbc:mysql://develop-db.rakenapp.com/raken";
      user = getConsulValue("config/application,nonprod/db.raken.username");
      password = getConsulValue("config/application,nonprod/db.raken.password");
    } else {
      String dbHost = getConsulValue("config/application," + env + "/db.host");
      dbUrl = "jdbc:mysql://" + dbHost + "/raken?useUnicode=true"; 
      user = getConsulValue("config/application," + env + "/db.username");
      password = getConsulValue("config/application," + env + "/db.password");
    }
  }

  private String getConsulValue(String property) {
    Consul client = Consul.builder().withUrl(CONSUL_URL).build();
    KeyValueClient kvClient = client.keyValueClient();
    String value = kvClient.getValue(property).get().getValueAsString().get();
    System.out.println("Retrieved a value for " + property + " from consul");
    return value;
  }
  
  private void runTests() throws Exception {
    try (Connection conn = DriverManager.getConnection(dbUrl, user, password)) {
      tests.forEach(test->test.run(conn));
    } 
  }
}
