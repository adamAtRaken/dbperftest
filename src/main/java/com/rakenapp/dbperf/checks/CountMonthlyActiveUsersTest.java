package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CountMonthlyActiveUsersTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "SELECT sum(total.cnt)\n" + 
      "FROM (SELECT DATE(s.timestamp) AS date, count(DISTINCT s.user_uuid) AS cnt\n" + 
      "FROM session s\n" + 
      "JOIN businesses bus ON s.business_uuid = bus.uuid\n" + 
      "WHERE bus.business_id = ?\n" + 
      "AND (MONTH(DATE(s.timestamp)) = MONTH(CURRENT_DATE()) AND YEAR(DATE(s.timestamp)) = YEAR(CURRENT_DATE()))\n" + 
      "UNION DISTINCT SELECT DATE(u.create_date) AS date, count(DISTINCT u.user_id) AS cnt\n" + 
      "FROM users u\n" + 
      "JOIN user_licenses ul ON ul.user_id = u.user_id\n" + 
      "JOIN businesses bus ON ul.business_id = bus.business_id\n" + 
      "WHERE ul.pending = 0\n" + 
      "AND ul.active = 1\n" + 
      "AND ul.deleted = 0\n" + 
      "AND ul.type = 'user_license'\n" + 
      "AND bus.business_id = ?\n" + 
      "AND u.super_user = 0\n" + 
      "AND u.last_login IS NULL\n" + 
      "AND MONTH(DATE(u.create_date)) = MONTH(CURRENT_DATE())\n" + 
      "AND YEAR(DATE(u.create_date)) = YEAR(CURRENT_DATE())\n" + 
      "AND u.uuid NOT IN\n" + 
      "(SELECT sess.user_uuid FROM session sess WHERE sess.business_uuid = bus.uuid)) total;";

  private static final String PARAMS_SQL = "select business_id from projects order by rand() limit " + NUM_RUNS;
  
  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    try (PreparedStatement ps = conn.prepareStatement(PARAMS_SQL)) {
      int index = 0;
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        results[index++][0] = rs.getString(1);
      }
    }
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6359 - Count Active Users";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ps.setString(1, params[0]);
      ps.setString(2, params[0]);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
