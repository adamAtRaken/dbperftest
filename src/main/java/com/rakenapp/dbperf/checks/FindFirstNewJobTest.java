package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FindFirstNewJobTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "select this_.job_request_id as job_requ2_41_0_,\n" + 
      "this_.emails as emails3_41_0_,\n" + 
      "this_.job_submission_date_time as job_subm4_41_0_,\n" + 
      "this_.last_status_update_date_time as last_sta5_41_0_,\n" + 
      "this_.priority as priority6_41_0_,\n" + 
      "this_.status as status7_41_0_,\n" + 
      "this_.urls as urls8_41_0_,\n" + 
      "this_.from_date as from_dat9_41_0_,\n" + 
      "this_.project_ids as project10_41_0_,\n" + 
      "this_.to_date as to_date11_41_0_,\n" + 
      "this_.project_id as project12_41_0_,\n" + 
      "this_.subcollaborator as subcoll13_41_0_,\n" + 
      "this_.business_id as busines14_41_0_,\n" + 
      "this_.payload as payload15_41_0_,\n" + 
      "this_.include_attachments as include16_41_0_,\n" + 
      "this_.statuses as statuse17_41_0_,\n" + 
      "this_.month as month18_41_0_,\n" + 
      "this_.week as week19_41_0_,\n" + 
      "this_.year as year20_41_0_,\n" + 
      "this_.type as type1_41_0_\n" + 
      "from job_request this_\n" + 
      "where this_.status = 'new'\n" + 
      "order by this_.priority desc\n" + 
      "limit 1";

  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6358 - Find First Job Request";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        // This is fine for this test - there may not be a ready job, as it should have been processed already.
        //throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
