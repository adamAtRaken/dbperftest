package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MediaCountTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "select count(distinct mds.media_id) as count from ( select distinct wlm.media_id from work_log_media wlm inner join work_logs wl on wl.work_log_id=wlm.work_log_id inner join reports r on r.report_id = wl.report_id inner join projects p on r.project_id = p.project_id where p.business_id = 18895 union all select distinct wlm.media_id from comment_media wlm inner join comments wl on wl.comment_id=wlm.comment_id inner join reports r on r.report_id = wl.report_id inner join projects p on r.project_id = p.project_id where p.business_id = 18895 union all select distinct wlm.media_id from task_media wlm inner join tasks wl on wl.task_id=wlm.task_id inner join projects p on p.project_id = wl.project_id where p.business_id = 18895 union all select distinct wlm.media_id from report_media wlm inner join reports wl on wl.report_id=wlm.report_id inner join projects p on p.project_id = wl.project_id where p.business_id = 18895 union all select distinct wlm.media_id from report_answers_media wlm inner join report_answers ra on ra.answer_id = wlm.answer_id inner join reports wl on wl.report_id=ra.report_id inner join projects p on p.project_id = wl.project_id where p.business_id = ?) mds";

  private static final String PARAMS_SQL = "select business_id from projects order by rand() limit " + NUM_RUNS;

  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    try (PreparedStatement ps = conn.prepareStatement(PARAMS_SQL)) {
      int index = 0;
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        results[index++][0] = rs.getString(1);
      }
    }
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6129 - Media Count";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ps.setString(1, params[0]);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
