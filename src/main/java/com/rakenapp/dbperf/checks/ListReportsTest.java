package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListReportsTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "select report0_.report_id as report_i1_66_," + 
      "report0_.create_date as create_d2_66_," + 
      "report0_.created_by as created_9_66_," + 
      "report0_.modified_by as modifie10_66_," + 
      "report0_.modify_date as modify_d3_66_," + 
      "report0_.report_date as report_d4_66_," + 
      "report0_.project_id as project11_66_," + 
      "report0_.signature as signatur5_66_," + 
      "report0_.signed_by as signed_12_66_," + 
      "report0_.signed_date as signed_d6_66_," + 
      "report0_.report_status as report_s7_66_," + 
      "report0_.ua as ua8_66_ " + 
      "from reports report0_ " + 
      "inner join projects abstractpr1_ on report0_.project_id = abstractpr1_.project_id " + 
      "inner join report_answers answers2_ on report0_.report_id = answers2_.report_id and (answers2_.tf_answer = 1) " + 
      "inner join report_questions reportques3_ on answers2_.question_id = reportques3_.question_id " + 
      "where 1 = 1 " + 
      "and abstractpr1_.status = 'ACTIVE' " + 
      "and abstractpr1_.business_id = ? " + 
      "and report0_.report_date >= '2017-09-29' " + 
      "and report0_.report_date <= '2018-09-29' " + 
      "and (reportques3_.tag in (3)) " + 
      "order by report0_.report_date desc"; 

  private static final String PARAMS_SQL = "select abstractpr1_.business_id " +
      "from reports report0_ " + 
      "inner join projects abstractpr1_ on report0_.project_id = abstractpr1_.project_id " + 
      "inner join report_answers answers2_ on report0_.report_id = answers2_.report_id and (answers2_.tf_answer = 1) " + 
      "inner join report_questions reportques3_ on answers2_.question_id = reportques3_.question_id " +
      "where 1 = 1 " + 
      "and abstractpr1_.status = 'ACTIVE' " + 
      "and report0_.report_date >= '2017-09-29' " + 
      "and report0_.report_date <= '2018-09-29' " + 
      "and (reportques3_.tag in (3)) " + 
      "order by rand() limit " + NUM_RUNS;

  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    try (PreparedStatement ps = conn.prepareStatement(PARAMS_SQL)) {
      int index = 0;
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        results[index++][0] = rs.getString(1);
      }
    }
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6357 - List Reports";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ps.setString(1, params[0]);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
