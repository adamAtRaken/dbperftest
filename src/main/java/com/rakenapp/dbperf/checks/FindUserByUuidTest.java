package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FindUserByUuidTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "select this_.user_id as user_id1_82_10_,\n" + 
      "this_.auth_token as auth_tok2_82_10_,\n" + 
      "this_.closeio_id as closeio_3_82_10_,\n" + 
      "this_.create_date as create_d4_82_10_,\n" + 
      "this_.created_by as created29_82_10_,\n" + 
      "this_.modified_by as modifie30_82_10_,\n" + 
      "this_.modify_date as modify_d5_82_10_,\n" + 
      "this_.dob as dob6_82_10_,\n" + 
      "this_.fb_access_token as fb_acces7_82_10_,\n" + 
      "this_.fb_expiration_date as fb_expir8_82_10_,\n" + 
      "this_.fb_id as fb_id9_82_10_,\n" + 
      "this_.fb_sharing_enabled as fb_shar10_82_10_,\n" + 
      "this_.gender as gender11_82_10_,\n" + 
      "this_.intercom_hibernated as interco12_82_10_,\n" + 
      "this_.last_login as last_lo13_82_10_,\n" + 
      "this_.last_login_ip as last_lo14_82_10_,\n" + 
      "this_.middle_name as middle_15_82_10_,\n" + 
      "this_.nickname as nicknam16_82_10_,\n" + 
      "this_.password as passwor17_82_10_,\n" + 
      "this_.phone as phone18_82_10_,\n" + 
      "this_.prefix as prefix19_82_10_,\n" + 
      "this_.suffix as suffix21_82_10_,\n" + 
      "this_.super_user as super_u22_82_10_,\n" + 
      "this_.title as title23_82_10_,\n" + 
      "this_.receive_newsletter as receive24_82_10_,\n" + 
      "this_.receive_offers as receive25_82_10_,\n" + 
      "this_.signature as signatu26_82_10_,\n" + 
      "this_.username as usernam27_82_10_,\n" + 
      "this_.uuid as uuid28_82_10_,\n" + 
      "userremind2_.id as id2_56_0_,\n" + 
      "userremind2_.remind_date as remind_d3_56_0_,\n" + 
      "userremind2_.user_id as user_id6_56_0_,\n" + 
      "userlicens3_.id as id2_80_1_,\n" + 
      "userlicens3_.active as active3_80_1_,\n" + 
      "userlicens3_.billable as billable4_80_1_,\n" + 
      "userlicens3_.business_id as busines11_80_1_,\n" + 
      "userlicens3_.create_date as create_d5_80_1_,\n" + 
      "userlicens3_.created_by as created12_80_1_,\n" + 
      "userlicens3_.modified_by as modifie13_80_1_,\n" + 
      "userlicens3_.modify_date as modify_d6_80_1_,\n" + 
      "userlicens3_.deleted as deleted7_80_1_,\n" + 
      "userlicens3_.pending as pending8_80_1_,\n" + 
      "userlicens3_.role_id as role_id14_80_1_,\n" + 
      "userlicens3_.end_date as end_date9_80_1_,\n" + 
      "userlicens3_.start_date as start_d10_80_1_,\n" + 
      "userlicens3_.user_id as user_id16_80_1_,\n" + 
      "business4_.business_id as business1_26_2_,\n" + 
      "business4_.account_type as account_2_26_2_,\n" + 
      "business4_.address_id as address22_26_2_,\n" + 
      "business4_.api_key as api_key3_26_2_,\n" + 
      "business4_.billing_platform as billing_4_26_2_,\n" + 
      "business4_.acq_campaign as acq_camp5_26_2_,\n" + 
      "business4_.closeio_id as closeio_6_26_2_,\n" + 
      "business4_.create_date as create_d7_26_2_,\n" + 
      "business4_.created_by as created23_26_2_,\n" + 
      "business4_.modified_by as modifie24_26_2_,\n" + 
      "business4_.modify_date as modify_d8_26_2_,\n" + 
      "business4_.credit_card_id as credit_c9_26_2_,\n" + 
      "business4_.credit_card_update_status as credit_10_26_2_,\n" + 
      "business4_.customer_id as custome11_26_2_,\n" + 
      "business4_.domain as domain12_26_2_,\n" + 
      "business4_.acq_medium as acq_med13_26_2_,\n" + 
      "business4_.name as name14_26_2_,\n" + 
      "business4_.phone as phone15_26_2_,\n" + 
      "business4_.qb_id as qb_id16_26_2_,\n" + 
      "business4_.referrer as referre17_26_2_,\n" + 
      "business4_.business_rep_id as busines25_26_2_,\n" + 
      "business4_.acq_source as acq_sou18_26_2_,\n" + 
      "business4_.status as status19_26_2_,\n" + 
      "business4_.ua as ua20_26_2_,\n" + 
      "business4_.uuid as uuid21_26_2_,\n" + 
      "address5_.address_id as address_1_17_3_,\n" + 
      "address5_.city as city2_17_3_,\n" + 
      "address5_.create_date as create_d3_17_3_,\n" + 
      "address5_.created_by as created15_17_3_,\n" + 
      "address5_.modified_by as modifie16_17_3_,\n" + 
      "address5_.modify_date as modify_d4_17_3_,\n" + 
      "address5_.country_id as country_5_17_3_,\n" + 
      "address5_.latitude as latitude6_17_3_,\n" + 
      "address5_.longitude as longitud7_17_3_,\n" + 
      "address5_.postal_code as postal_c8_17_3_,\n" + 
      "address5_.state as state9_17_3_,\n" + 
      "address5_.status as status10_17_3_,\n" + 
      "address5_.street_address_1 as street_11_17_3_,\n" + 
      "address5_.street_address_2 as street_12_17_3_,\n" + 
      "address5_.time_zone as time_zo13_17_3_,\n" + 
      "address5_.type as type14_17_3_,\n" + 
      "businessre6_.id as id1_25_4_,\n" + 
      "businessre6_.name as name2_25_4_,\n" + 
      "businesspr7_.pref_id as pref_id1_24_5_,\n" + 
      "businesspr7_.business_id as business8_24_5_,\n" + 
      "businesspr7_.create_date as create_d2_24_5_,\n" + 
      "businesspr7_.created_by as created_9_24_5_,\n" + 
      "businesspr7_.modified_by as modifie10_24_5_,\n" + 
      "businesspr7_.modify_date as modify_d3_24_5_,\n" + 
      "businesspr7_.fahrenheit as fahrenhe4_24_5_,\n" + 
      "businesspr7_.primary_color as primary_5_24_5_,\n" + 
      "businesspr7_.secondary_color as secondar6_24_5_,\n" + 
      "businesspr7_.week_first_day as week_fir7_24_5_,\n" + 
      "role8_.role_id as role_id1_68_6_,\n" + 
      "role8_.create_date as create_d2_68_6_,\n" + 
      "role8_.created_by as created_7_68_6_,\n" + 
      "role8_.modified_by as modified8_68_6_,\n" + 
      "role8_.modify_date as modify_d3_68_6_,\n" + 
      "role8_.role_desc as role_des4_68_6_,\n" + 
      "role8_.display_name as display_5_68_6_,\n" + 
      "role8_.role_name as role_nam6_68_6_,\n" + 
      "role8_.parent_id as parent_i9_68_6_,\n" + 
      "children9_.parent_id as parent_i9_68_12_,\n" + 
      "children9_.role_id as role_id1_68_12_,\n" + 
      "children9_.role_id as role_id1_68_7_,\n" + 
      "children9_.create_date as create_d2_68_7_,\n" + 
      "children9_.created_by as created_7_68_7_,\n" + 
      "children9_.modified_by as modified8_68_7_,\n" + 
      "children9_.modify_date as modify_d3_68_7_,\n" + 
      "children9_.role_desc as role_des4_68_7_,\n" + 
      "children9_.display_name as display_5_68_7_,\n" + 
      "children9_.role_name as role_nam6_68_7_,\n" + 
      "children9_.parent_id as parent_i9_68_7_,\n" + 
      "worker10_.worker_id as worker_i1_87_8_,\n" + 
      "worker10_.business_id as busines11_87_8_,\n" + 
      "worker10_.classification_id as classif12_87_8_,\n" + 
      "worker10_.code as code2_87_8_,\n" + 
      "worker10_.eid as eid3_87_8_,\n" + 
      "worker10_.email as email4_87_8_,\n" + 
      "worker10_.first_name as first_na5_87_8_,\n" + 
      "worker10_.last_name as last_nam6_87_8_,\n" + 
      "worker10_.phone_number as phone_nu7_87_8_,\n" + 
      "worker10_.status as status8_87_8_,\n" + 
      "worker10_.user_id as user_id13_87_8_,\n" + 
      "worker10_.uuid as uuid9_87_8_,\n" + 
      "worker10_.worker_type as worker_10_87_8_,\n" + 
      "classifica11_.classification_id as classifi1_30_9_,\n" + 
      "classifica11_.business_id as business5_30_9_,\n" + 
      "classifica11_.name as name2_30_9_,\n" + 
      "classifica11_.status as status3_30_9_,\n" + 
      "classifica11_.type as type4_30_9_\n" + 
      "from users this_\n" + 
      "left outer join reminder userremind2_ on this_.user_id = userremind2_.user_id and userremind2_.type = 'user'\n" + 
      "left outer join user_licenses userlicens3_\n" + 
      "on this_.user_id = userlicens3_.user_id and userlicens3_.type = 'user_license'\n" + 
      "left outer join businesses business4_ on userlicens3_.business_id = business4_.business_id\n" + 
      "left outer join addresses address5_ on business4_.address_id = address5_.address_id\n" + 
      "left outer join business_reps businessre6_ on business4_.business_rep_id = businessre6_.id\n" + 
      "left outer join business_preferences businesspr7_ on business4_.business_id = businesspr7_.business_id\n" + 
      "left outer join roles role8_ on userlicens3_.role_id = role8_.role_id\n" + 
      "left outer join roles children9_ on role8_.role_id = children9_.parent_id\n" + 
      "left outer join workers worker10_ on this_.user_id = worker10_.user_id\n" + 
      "left outer join classifications classifica11_ on worker10_.classification_id = classifica11_.classification_id\n" + 
      "where this_.uuid = ?";

  private static final String PARAMS_SQL = "select uuid from users order by rand() limit " + NUM_RUNS;
  
  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    try (PreparedStatement ps = conn.prepareStatement(PARAMS_SQL)) {
      int index = 0;
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        results[index++][0] = rs.getString(1);
      }
    }
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6360 - Find User by UUID";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ps.setString(1, params[0]);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
