package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InvoiceIndexesTest extends AbstractPerformanceTest {
  private static final String QUERY_SQL = "select invoice0_.invoice_id AS invoice_1_39_, invoice0_.amount AS amount2_39_, invoice0_.comment AS comment3_39_, invoice0_.create_date AS create_d4_39_, invoice0_.created_by AS created33_39_, invoice0_.modified_by AS modifie34_39_, invoice0_.modify_date AS modify_d5_39_, invoice0_.currency_code AS currency6_39_, invoice0_.invoice_date AS invoice_7_39_, invoice0_.send_state AS send_sta8_39_, invoice0_.new_auto_renew AS new_auto9_39_, invoice0_.new_plan_id AS new_pla35_39_, invoice0_.new_billing_platform AS new_bil10_39_, invoice0_.new_discount AS new_dis11_39_, invoice0_.new_period_type AS new_per12_39_, invoice0_.new_subscription_end_date AS new_sub13_39_, invoice0_.new_subscription_seats AS new_sub14_39_, invoice0_.new_subscription_start_date AS new_sub15_39_, invoice0_.new_subscription_state AS new_sub16_39_, invoice0_.new_unit_price AS new_uni17_39_, invoice0_.num_licenses AS num_lic18_39_, invoice0_.old_auto_renew AS old_aut19_39_, invoice0_.old_plan_id AS old_pla36_39_, invoice0_.old_billing_platform AS old_bil20_39_, invoice0_.old_discount AS old_dis21_39_, invoice0_.old_period_type AS old_per22_39_, invoice0_.old_subscription_end_date AS old_sub23_39_, invoice0_.old_subscription_seats AS old_sub24_39_, invoice0_.old_subscription_start_date AS old_sub25_39_, invoice0_.old_subscription_state AS old_sub26_39_, invoice0_.old_unit_price AS old_uni27_39_, invoice0_.payment_id AS payment37_39_, invoice0_.percentage_left AS percent28_39_, invoice0_.pro_rate AS pro_rat29_39_, invoice0_.status AS status30_39_, invoice0_.subscription_id AS subscri38_39_, invoice0_.type AS type31_39_, invoice0_.uuid AS uuid32_39_ FROM invoice invoice0_ WHERE invoice0_.uuid=? FOR update";

  private static final String PARAMS_SQL = "select uuid from invoice order by rand() limit " + NUM_RUNS;

  @Override
  protected String[][] getQueryParams(Connection conn) throws SQLException {
    String[][] results = new String[NUM_RUNS][1];
    try (PreparedStatement ps = conn.prepareStatement(PARAMS_SQL)) {
      int index = 0;
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        results[index++][0] = rs.getString(1);
      }
    }
    return results;
  }

  @Override
  protected String getTestName() {
    return "RAK-6204 - Invoice Indexes";
  }

  @Override
  protected void performTest(Connection conn, String[] params) throws Exception {
    try (PreparedStatement ps = conn.prepareStatement(QUERY_SQL)) {
      ps.setString(1, params[0]);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        throw new Exception("The test failed - there was no result from this query");
      }
    }
  }
}
