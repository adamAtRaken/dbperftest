package com.rakenapp.dbperf.checks;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public abstract class AbstractPerformanceTest {
  public static final int NUM_RUNS = 20;
  private SummaryStatistics stats;
  
  public AbstractPerformanceTest() {
    stats = new SummaryStatistics();
  }
  
  public void run(Connection conn) {
    String message = "Starting " + getTestName();
    String underline = StringUtils.repeat('=', message.length());
    System.out.println();
    System.out.println();
    System.out.println(message);
    System.out.println(underline);
    System.out.println();
    
      try {
        String[][] params = getQueryParams(conn);
        
        for(int i = 0; i<NUM_RUNS; i++) {
          long start = System.currentTimeMillis(); 
          performTest(conn, params[i]);
          long finish = System.currentTimeMillis();
          stats.addValue(finish - start);
        }
      } catch (Exception e) {
        System.out.println("Exception during test run: " + e.getMessage());
        e.printStackTrace();
      }
      
      report();
  }

  private void report() {
    System.out.println(stats.getSummary());
  }

  protected abstract void performTest(Connection conn, String[] params) throws Exception;

  protected abstract String[][] getQueryParams(Connection conn) throws SQLException;

  protected abstract String getTestName();
}
